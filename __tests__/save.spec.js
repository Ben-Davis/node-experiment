/* @flow */
jest.mock('fs', () => ({ writeFile: (path, mode, callback) => {
  if (path === 'break/break.') {
    return callback('error')
  }
  return callback(null)
},
}))
jest.mock('../createHashFolders', () => (path, hash) => {
  if (hash === 'break') {
    return Promise.resolve(hash)
  }
  return Promise.resolve('1234')
})

const save = require('../save')

describe('save function', () => {
  it('should return the path in the promise', (done) => {
    const resizedImage = {
      toBuffer: jest.fn(() => Promise.resolve('test')),
    }
    const expected = '1234/hash.'
    save(resizedImage, '', 'hash', '').then((saveReturn) => {
      expect(expected).toBe(saveReturn)
      done()
    })
  })
  it('should return an error if there is one', (done) => {
    const resizedImage = {
      toBuffer: jest.fn(() => Promise.resolve('test')),
    }
    save(resizedImage, '', 'break', '').then((saveReturn) => {
    }).catch(err => {
      expect(err).toBe('error')
      done()
    })
  })
})
