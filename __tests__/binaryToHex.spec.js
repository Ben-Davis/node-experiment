/* @flow */
const binaryToHex = require('../binaryToHex')

describe('binaryToHex', () => {
  it('should return the right hex value when we pass a binary valuej', () => {
    const binaryToHexReturn = binaryToHex('000111111000')
    const expected = '1f8'
    expect(binaryToHexReturn).toBe(expected)
  })
})
