/* @flow */

jest.mock('sharp-phash', () => (hash) => Promise.resolve(hash))
jest.mock('../binaryToHex', () => (buffer) => buffer)
const getHash = require('../getHash')

describe('getHash function', () => {
  it('should return the hash given', (done) => {
    getHash('buffer').then(hash => {
      expect(hash).toBe('buffer')
      done()
    })
  })
})
