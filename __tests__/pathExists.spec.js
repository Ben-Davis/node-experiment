/* @flow */

jest.mock('fs', () => ({ stat: (path, callback) => {
  if (path === '../path') {
    callback(true, null)
  } else {
    callback(null, true)
  }
},
}))
const pathExists = require('../pathExists')

describe('pathExists function', () => {
  it('should resolve with false when the path doesn\'t exist', (done) => {
    pathExists('../path').then(res => {
      expect(res).toBe(false)
      done()
    })
  })
  it('should resolve with true when the path exists', (done) => {
    pathExists('../').then(res => {
      expect(res).toBe(true)
      done()
    })
  })
})
