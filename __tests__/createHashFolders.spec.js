/* @flow */
jest.mock('fs', () => ({ mkdir: (path, mode, callback) => {
  if (path === 'error/') {
    return callback('error')
  }
  if (path === 'exist-error/') {
    return callback({ code: 'EEXIST' })
  }
  if (path === 'exist-error//') {
    return callback({ code: 'EEXIST' })
  }
  if (path === 'exist-error/do') {
    return callback({ code: 'EEXIST' })
  }
  if (path === 'exist-error/ya') {
    return callback({ code: 'EEXIST' })
  }
  if (path === 'exist-error/do/no') {
    return callback('break')
  }
  if (path === 'fine/yi/ni') {
    return callback('error')
  }
  return callback(null)
},
}))
const createHashFolder = require('../createHashFolders')

describe('createHashFolders', () => {
  it('should throw an error when the promise is rejected', (done) => {
    // $FlowIgnore
    createHashFolder('dave', { key: 'dave' }).then(res => {
      expect(res).toBe('dave/11/22/')
    }).catch((err: Error) => {
      expect(err).toBeDefined()
      done()
    })
  })
  it('should return the path when the promise is resolved', (done) => {
    createHashFolder('dave', '1122').then(res => {
      expect(res).toBe('dave/11/22')
      done()
    }).catch((err: Error) => {
      return err
    })
  })
  it('should return an error when present', (done) => {
    createHashFolder('error', '').then(res => {
    }).catch((err: Error) => {
      expect(err).toBe('error')
      done()
    })
  })
  it('should return an EEXIST error when present', (done) => {
    createHashFolder('exist-error', '').then(res => {
      expect(res).toBe('exist-error//')
      done()
    }).catch((err: Error) => {
      return err
    })
  })
  it('should return an EEXIST error when present', (done) => {
    createHashFolder('exist-error', 'dono').then(res => {
    }).catch((err: Error) => {
      expect(err).toBe('break')
      done()
    })
  })
  it('should return an EEXIST error when present', (done) => {
    createHashFolder('exist-error', 'yano').then(res => {
      expect(res).toBe('exist-error/ya/no')
      done()
    }).catch((err: Error) => {
      return err
    })
  })
  it('should return an EEXIST error when present', (done) => {
    createHashFolder('fine', 'yini').then(res => {
    }).catch((err: Error) => {
      expect(err).toBe('error')
      done()
    })
  })
})
