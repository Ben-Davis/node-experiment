/* @flow */
const scale = require('../scale')

describe('scale function', () => {
  it('should return the image if no max values are specified', () => {
    const image = { width: 1, height: 2 }
    const scaleReturn = scale(image)
    expect(scaleReturn).toEqual(image)
  })
  it('should return the image if it is smaller than the max values specified', () => {
    const image = { width: 1, height: 2 }
    const max = { width: 3, height: 4 }
    const scaleReturn = scale(image, max)
    expect(scaleReturn).toEqual(image)
  })
  it('should lower the width of the image to the max width if it is bigger than the max width', () => {
    const image = { width: 3, height: 2 }
    const max = { width: 2, height: 3 }
    const expected = { width: 2 }
    const scaleReturn = scale(image, max)
    expect(scaleReturn).toEqual(expected)
  })
  it('should lower the height of the image to the max height if it is bigger than the max height', () => {
    const image = { width: 2, height: 3 }
    const max = { width: 3, height: 2 }
    const expected = { height: 2 }
    const scaleReturn = scale(image, max)
    expect(scaleReturn).toEqual(expected)
  })
})
