/* @flow */
const phash = require('sharp-phash')
const binaryToHex = require('./binaryToHex')
const defaultHash = require('./defaultProcesses/defaultHashProcess')

module.exports = function getHash(buffer: any, m) { // TODO Type Buffer
  if (m.includes('image')) {
    return phash(buffer).then((hash) => {
      return binaryToHex(hash)
    })
  }
  return defaultHash().generateHash(buffer)
}
