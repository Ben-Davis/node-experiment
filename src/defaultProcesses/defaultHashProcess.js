/* @flow */

const crypto = require('crypto')

const resolve = (x) => Promise.resolve(x)

function defaultHashProcess(file: Buffer) {
  return new Promise((resolve, reject) =>
  resolve(crypto
    .createHash('md5')
    .update(file)
    .digest('hex'))
  )
}

module.exports = () => ({
  modify: resolve,
  generateHash: defaultHashProcess,
})
