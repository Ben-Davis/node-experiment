/* @flow */
const fs = require('fs')

const createHashFolder = require('./createHashFolders')

module.exports = function save(resizedFile: any, rootPath: string, hash: string, format: string): Promise<string> {
  return new Promise((resolve, reject) => createHashFolder(`${rootPath}`, hash).then(path => {
    const uploadedPath = `${path}/${hash}.${format}`
    fs.writeFile(uploadedPath, resizedFile, (err) => {
      if (err) {
        return reject(err)
      }
      return resolve(uploadedPath)
    })
    return resolve(uploadedPath)
  }))
}
