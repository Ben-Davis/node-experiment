/* @flow */
const fs = require('fs')

module.exports = function pathExists(path: string) {
  return new Promise((resolve, reject) => fs.stat(path, (err, stats) => {
    if (stats && !err) {
      return resolve(true)
    } else {
      return resolve(false)
    }
  }))
}
