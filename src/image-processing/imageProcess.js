/* @flow */
const resize = require('../resize')
const getHash = require('../getHash')
import type { ImageSettings } from '../../types'

const resizeImage = (imageSettings: ImageSettings) => (file: Buffers) => resize(file, imageSettings)

module.exports = (settings) => ({
  modify: resizeImage(settings),
  generateHash: getHash,
})
