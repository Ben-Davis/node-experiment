/* @flow */
const fs = require('fs')
const EEXIST = 'EEXIST'
module.exports = function createHashFolder(path: string, hash: string) {
  return new Promise((resolve, reject) => fs.mkdir(`${path}/${hash.slice(0, 2)}`, 0o777, (err) => {
    if (err) {
      if (err.code === EEXIST) {
        return fs.mkdir(`${path}/${hash.slice(0, 2)}/${hash.slice(2, 4)}`, 0o777, (err) => {
          if (err) {
            if (err.code === EEXIST) {
              return resolve(`${path}/${hash.slice(0, 2)}/${hash.slice(2, 4)}`)
            }
            return reject(err)
          }
          return resolve(`${path}/${hash.slice(0, 2)}/${hash.slice(2, 4)}`)
        })
      }
      return reject(err)
    }
    return fs.mkdir(`${path}/${hash.slice(0, 2)}/${hash.slice(2, 4)}`, 0o777, (err) => {
      if (err) {
        return reject(err)
      }
      return resolve(`${path}/${hash.slice(0, 2)}/${hash.slice(2, 4)}`)
    })
  })
)
}
