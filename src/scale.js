/* @flow */

type Maxs = {
  width: ?number,
  height: ?number,
}
type ImageDimensions = {
  width: number,
  height: number,
}
type ScaledImageDimensions = { +width: ?number, +height: ?number }

module.exports = function scale(image: ImageDimensions, max: Maxs): ScaledImageDimensions {
  const maxWidth = max.width || image.width
  const maxHeight = max.height || image.height
  if ((!max.width && !max.height) || (image.width <= maxWidth && image.height <= maxHeight)) {
    return image
  }
  const imageRatio = image.width / image.height
  const maxRatio = maxWidth / maxHeight
  return {
    width: imageRatio < maxRatio ? null : maxWidth,
    height: imageRatio < maxRatio ? maxHeight : null,
  }
}
