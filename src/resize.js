/* @flow */
const sharp = require('sharp')
const scale = require('./scale')

import type { ImageSettings } from '../types'

module.exports = function resize(buffer: Buffer, maxs: ImageSettings) {
  const maxHeight = !isNaN(maxs.maxHeight) ? parseInt(maxs.maxHeight) : null
  const maxWidth = !isNaN(maxs.maxWidth) ? parseInt(maxs.maxWidth) : null
  return sharp(buffer).metadata().then((metadata) => {
    const { width, height } = scale(
        { height: parseInt(metadata.height), width: parseInt(metadata.width) },
        { height: maxHeight, width: maxWidth }
      )
    return sharp(buffer).resize(width, height).toBuffer().then((buffer) => buffer)
  })
}
