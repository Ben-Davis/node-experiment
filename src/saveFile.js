/* @flow */
const mime = require('mime-types')
const pathExists = require('./pathExists')
const save = require('./save')

import type { MulterFile, ProcessFile, ResultFile, Settings } from '../types'

function hashToPath(rootPath, hash, extension) {
  return `.${rootPath}/` +
  hash.slice(0, 2) + '/' +
  hash.slice(2, 4) + '/' +
  `${hash}.${extension}`
}

module.exports = function saveFile(
  rootPath: string,
  transform: Buffer => Buffer,
  generateHash: Buffer => string
): (file: MulterFile) => Promise<ResultFile> {
  return async ({ buffer, ...file }: MulterFile): Promise<ResultFile> => {
    const extension = mime.extension(file.mimetype)
    const fileHash: string = await generateHash(buffer, file.mimetype)
    const path: string = hashToPath(rootPath, fileHash, extension)
    if (await pathExists(path)) {
      return { ...file, path }
    }
    return transform(buffer, file.mimetype)
    .then(file => save(file, rootPath, fileHash, extension))
    .then(path => ({ ...file, path }))
  }
}
