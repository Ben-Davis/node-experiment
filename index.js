/* @flow */
const multer = require('multer')
const express = require('express')
const app = express()
const upload = multer({ storage: multer.memoryStorage() })
const cors = require('cors')

const defaultProcess = require('./src/defaultProcesses/defaultHashProcess')
const imageProcess = require('./src/image-processing/imageProcess')
const getHash = require('./src/getHash')

const saveFile = require('./src/saveFile')
import type { ResultFile, ProcessFile } from './types'

app.use(cors())
app.use(express.static('public'))

app.get('/', (req, res) => {
  res.send('Hello World')
})

const imageMimeTypes = {
  'image/jpg': 'image/jpg',
  'image/jpeg': 'image/jpeg',
  'image/gif': 'image/gif',
  'image/svg': 'image/svg',
}

export type FileMimeType = $Keys<typeof imageMimeTypes>

const process = (settings) => (b: Buffer, m) => {
  console.log(m)
  if (m.includes('image')) {
    return imageProcess(settings).modify(b)
  }
  return defaultProcess(settings).modify(b)
}

const middleware = (rootPath, process: { [key:string]: ProcessFile }) => async (req, res) => {
  Promise.all(req.files.map(saveFile(rootPath, process(req.body), getHash))).then((files: Array<ResultFile>) => {
    res.send(files)
  })
}

app.post('/multi', upload.array('files'), middleware('/Users/bendavis/Documents/web-experiments/node-uploads', process))

app.listen(8081, () => {
  /* eslint-disable no-console */
  console.log('Everything is awesome')
  /* eslint-enable no-console */
})
