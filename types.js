/* @flow */

export type FileSettings = {
  maxHeight: number,
  maxWidth: number,
}

export type FileBase = {
  fieldname: string,
  originalname: string,
  encoding: string,
  mimetype: string,
  size: number,
}

export type MulterFile = FileBase & {
  buffer: Buffer,
}

export type ResultFile = FileBase & {
  path: string,
}
export type ImageSettings = {
  maxWidth: ?string,
  maxHeight: ?string,
}
type EmptySettings = {}

export type Settings = ImageSettings | EmptySettings

export interface ProcessFile {
  modify(file: Buffer, settings: Settings): Promise<Buffer>,
  generateHash(file: Buffer): Promise<string>
}
